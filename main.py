from north_c9.axis import Axis
from north_c9.controller import C9Controller

# setup controller
controller = C9Controller()
# create a raw axis
axis = Axis(controller, 0)
# move axis 10,000 counts at 5,000 counts/sec
axis.move_counts_absolute(10000, velocity=5000, acceleration=5000)
